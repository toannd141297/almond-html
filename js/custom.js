var Almond = (function () {
  "use strict";
  /* Search Bar ============ */

  var handlePreloader = function () {
    setTimeout(function () {
      jQuery("#preloader").remove();
      $("#main-wrapper").addClass("show");
    }, 100);
  };
  var handleMenu = function () {
    var menu = $(".btn--menu");

    menu.click(function () {
      $(".menu-bottom").toggleClass("active");
      return false;
    });
  };

  /* Function ============ */
  return {
    init: function () {
      handleMenu();
    },
    load: function () {
      handlePreloader();
    },
  };
})();

/* Document.ready Start */
jQuery(document).ready(function () {
  Almond.init();
});
/* Document.ready END */

/* Window Load START */
jQuery(window).on("load", function () {
  Almond.load();
});
/*  Window Load END */
$(".hero--list").slick({
  dots: true,
  infinite: true,
  centerMode: true,

  // autoplay: true,
  // speed: 300,
  autoplaySpeed: 2000,
  centerPadding: "300px",
  slidesToShow: 1,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1500,
      settings: {
        centerPadding: "100px",
      },
    },
  ],
});
$(".multiple-items").slick({
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 4,
});

$(".photo-list").slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  speed: 1000,
  autoplaySpeed: 2000,
});

$(".time--list").slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  speed: 1000,
  autoplaySpeed: 2000,
});